<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
	<?php
		include '5-3.php';
	?>
 	<?php
 		//Declarations
	 	$fNameError = $emailError = $numberError = $genderError = $imageError = '';
	 	$fNameToValidate = $emailToValidate = $numberToValidate = $addressToValidate = $genderToValidate = '';

		//Check Session
	 	if(isset($_SESSION['username'])){	
		 	$username = $_SESSION['username'];
		 	echo '<b>user: ' . $username . '</b>&nbsp;&nbsp;&nbsp;&nbsp;'
		 		. '<a href="3-5_menu.php" class="btn btn-secondary">Main menu</a>&nbsp;'
		 		. '<a href="3-5_view.php" class="btn btn-primary">View data</a>&nbsp;'
		 		. '<a href="3-5_logout.php" class="btn btn-danger">Log Out</a>&nbsp;';
	 	} else {
	 		echo "no session";
	 	}

	 	//Destroy Session
 		if(isset($_POST['logout'])){
 			session_unset($_SESSION['username']);
 		}

 		//Add data
 		if (isset($_POST['submit'])) {
 			//Image Validation
	 		$rootUploads = 'uploads/';
			$target_file = $rootUploads . basename($_FILES['imageToUpload']['name']);
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			echo $target_file;
  			$identifyFile = getimagesize($_FILES["imageToUpload"]["tmp_name"]);
  			if($identifyFile !== false) {
				if($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg'
				|| $imageFileType == 'gif' ) {
					if ($_FILES['imageToUpload']['size'] < 1000000) {
						$imageError = '';
					} else {
						$imageError = 'Image too large.';
					}
				} else {
				  $imageError = 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
				}
		  	} else {
			    echo "File is not an image.";
		  	}

 			//Full name Vaidation
 			if (empty($_POST['fName'])) {
			    $fNameError = 'full name is required';
		  	} else {
			    $fNameToValidate = test_input($_POST['fName']);
			    if (!preg_match("/^[a-zA-Z-' ]*$/",$fNameToValidate)) {
			      $fNameError = 'Only letters and white space are allowed';
			    }
			}

			//Email Validation
		  	if (empty($_POST['email'])) {
		    	$emailError = 'Email is required';
		  	} else {
			    $emailToValidate = test_input($_POST['email']);

			    if (!filter_var($emailToValidate, FILTER_VALIDATE_EMAIL)) {
			   		$emailError = 'Incorrect email format';
			    }
		  	}

			//Number Validation
		  	if (empty($_POST['mNumber'])) {
		    	$numberError = 'mobile number is required';
		  	} else {
		    	$numberToValidate = test_input($_POST['mNumber']);

			    if (!is_numeric($_POST['mNumber'])) {
			   		$numberError = 'Incorrect mobile number format';
			    }
		  	}

			//Address Validation
		  	if (empty($_POST['address'])) {
		    	$address = '';
		  	} else {
		    	$addressToValidate = test_input($_POST['address']);
		  	}

			//Gender Validation
			if (empty($_POST['gender'])) {
			    $genderError = 'Gender is required';
		  	} else {
		    	$genderToValidate = test_input($_POST['gender']);
		  	}

		  	//Initialize Insert Query
	 		if ($fNameError == '' && $emailError == '' && $numberError == '' && $genderError == '' && $imageError == '') {
	 			auditCredentials($target_file);
	 		}
 		}

 		function auditCredentials($target_file) {
 			include "3-5_dbConnection.php";
 			$sql = "INSERT INTO `profiles` (`full_name`, `email`, `mobile_number`, `address`, `gender`, `image_location`) VALUES ('".$_POST['fName']."', '".$_POST['email']."', '".$_POST['mNumber']."', '".$_POST['address']."', '".$_POST['gender']."', '".$target_file."');";

		  	$result = $dbConn->query($sql);
			if($result == TRUE){
		  		move_uploaded_file($_FILES['imageToUpload']['tmp_name'], $target_file);
		  		echo '<p><span style="color:green;">* data and image added successfully</span></p>';
			}else{
				echo "Error: ".$sql. "<br>".$conn->error;
			}
			$dbConn->close();
 		}

	  	function test_input($data) {
			$data = trim($data);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			return $data;
		}
 	?>
	<div class="container">
		<h3> Add profile </h3>
		<p><span style='color:red;'>* required field</span></p>
		<form action="" method="post" enctype="multipart/form-data">
		    <label>Full Name: </label>&nbsp;
			<input type="text" name="fName">
			<span style='color:red;'>* <?php echo $fNameError;?></span>
			<br>
			<label>Email Address: </label>&nbsp;
			<input type="text" name="email">
			<span style='color:red;'>* <?php echo $emailError;?></span>
			<br>
			<label>Number: </label>&nbsp;
			<input type="text" name="mNumber">
			<span style='color:red;'>* <?php echo $numberError;?></span>
			<br>
			<label>Address: </label>&nbsp;
			<input type="text" name="address">
			<br><br>
			<input type="radio" id="male" name="gender" value="male">
			<label for="male">Male</label>
			<input type="radio" id="female" name="gender" value="female">
			<label for="female">Female</label>
			<input type="radio" id="other" name="gender" value="other">
			<label for="other">Other</label>
			<span style='color:red;'>* <?php echo $genderError;?></span>
			<br><br>
			Select image to upload:&nbsp;&nbsp;<span style='color:red;'> <?php echo $numberError;?></span>
	  		<input type="file" name="imageToUpload" id="imageToUpload">
			<br><br>
		    <button type="submit" name="submit" value="submit">submit</button>
		</form>
 	</div>
</body>
</html>