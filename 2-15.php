<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body onload="startTime()">
	<?php
		include '5-3.php';
	?>
	<p id="timeTick"></p>
	<script type="text/javascript">
		function startTime() {
	  		var today = new Date();
		  	var hr = today.getHours();
		  	var min = today.getMinutes();
		  	var sec = today.getSeconds();
		  	min = checkTime(min);
		  	sec = checkTime(sec);
		  	document.getElementById('timeTick').innerHTML = hr + ":" + min + ":" + sec;
		  	var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
	  		if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	  		return i;
		}
	</script>
</body>
</html>