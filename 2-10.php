<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
	<?php
		include '5-3.php';
	?>

    <button type="submit" name="submit" value="submit" id="srldn" onclick="scrollDown()">Scroll down</button>
    <div class="container">
    	<div class="card" style="height: 100rem;">
	  	<img class="card-img-top" src="..." alt="Card image cap">
		  	<div class="card-body">
			    <h5 class="card-title">Card title</h5>
			    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			    <a href="#" class="btn btn-primary">Go somewhere</a>
		  	</div>
		</div>
    </div>
    <button type="submit" name="submit" value="submit" id="srlup" onclick="scrollUp()">Scroll up</button>
    <br>
	<script type="text/javascript">
		function scrollDown(){
			window.scrollTo(100, 1000);
		}

		function scrollUp(){
			window.scrollTo(0, 0);
		}
	</script>
</body>
</html>