<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
	<script defer type="text/javascript" src="script.js"></script>
</head>
<body>
	<?php
		include '../5-3.php';
	?>
	<div class="container">
        <form id='form-result' class="hide" action="">
          <div class="SP-container">
            <div class="modal-text modal-text-bigger">Done!</div>
            <div class="modal-text">Right answers:</div>

            <div class="modal-text"><span id="right-answers" class="count"> 12 </span> of <span id="all-questions" class="count">count-of-questions</span> (<span id="answers-percent"></span>%)</div> 
          </div>
        </form>
		<div id="question_Container" class="hide">
			<div id="question_Text">[Question_Text]</div>
			<div id="choices" class="btn-grid">
				<!--
				<button class="btn">[Choice1]</button>
				<button class="btn">[Choice2]</button>
				<button class="btn">[Choice3]</button> -->
			</div>
		</div>
		<div class="controls">
			<button id="startBtn" class="start-btn btn">Start</button>
			<button id="nextBtn" class="next-btn btn hide">Next</button>
		</div>
	</div>
</body>
</html>