var startBtn = document.getElementById('startBtn');
var nextBtn = document.getElementById('nextBtn');
var questionContainerElement = document.getElementById('question_Container');
var resultForm = document.getElementById('form-result');

startBtn.addEventListener('click', startGame);
nextBtn.addEventListener('click', () => {
	currentQuestionIndex++;
	setNextQuestion();
})

var questionElement = document.getElementById('question_Text');
var answerButtonsElement = document.getElementById('choices');

var shuffledQuestions = 0, currentQuestionIndex = 0, cntCorrectAnswers = 0;
var questionToPush = [];
var questions = [];

function generateQuestionSet(){
	console.log('generate questions');
	$.ajax({
		url: 'questionnaires.php',
		type: 'get',
		dataType: 'json',
		success: function(response) {
			countRightAnswers = 0;
			console.log(response);
			questions = [];
			for (var i = 0; i < response.length; i++) {
				questionToPush.push({
					question: response[i].questionText,
					choices: [
						{ text: response[i].choice1, correct: validateChoice(response[i].choice1, response[i].answer) },
						{ text: response[i].choice2, correct: validateChoice(response[i].choice2, response[i].answer) },
						{ text: response[i].choice3, correct: validateChoice(response[i].choice3, response[i].answer) }
					],
					answer: response[i].answer
				});
				console.log(questionToPush[0]);
				questions.push(questionToPush[0]);
				questionToPush = [];
			}
			console.log('started');
			console.log(questions[0]);
			resetState();
			startBtn.classList.add('hide');
			shuffledQuestions = questions.sort(() => Math.random() - .5);
			currentQuestionIndex = 0;
			questionContainerElement.classList.remove('hide');
			showQuestion(shuffledQuestions[0]);
		}
	});
}

function validateChoice(choice, answer){
	if (choice == answer) {
		return true;
	} else {
		return false;
	}
}



function startGame(){
	generateQuestionSet();
}

function setNextQuestion() {
	resetState();
	console.log(shuffledQuestions[currentQuestionIndex]);
	showQuestion(shuffledQuestions[currentQuestionIndex]);
}

function showQuestion(question) {
	questionElement.innerText = question.question;
	question.choices.forEach(choices => {
		var btn = document.createElement('button');
		btn.innerText = choices.text;
		btn.classList.add('btn');
		if (choices.correct) {
			btn.dataset.correct = choices.correct;
		}
		btn.addEventListener('click', selectAnswer);
		answerButtonsElement.appendChild(btn);
	});
}

function resetState() {
	nextBtn.classList.add('hide');
	resultForm.classList.add('hide');
	while (answerButtonsElement.firstChild){
		answerButtonsElement.removeChild(answerButtonsElement.firstChild);
	}
}

function selectAnswer(e) {
	var selectedButton = e.target;
	var correct = selectedButton.dataset.correct;
	setStatusClass(document.body, correct);
	Array.from(answerButtonsElement.children).forEach(button => {
		setStatusClass(button, button.dataset.correct);
	});
	if (shuffledQuestions.length > currentQuestionIndex + 1){
		nextBtn.classList.remove('hide');
	} else {
		resultForm.classList.remove('hide');
		questionContainerElement.classList.add('hide');
	
		startBtn.innerText = 'Repeat'; 
		startBtn.classList.remove('hide');
	}
	if (selectedButton.dataset = correct) {
	  countRightAnswers++;
	}
	
	document.getElementById('right-answers').innerHTML = countRightAnswers; 
	document.getElementById('all-questions').innerHTML = questions.length; 
	document.getElementById('answers-percent').innerHTML = ((100 * countRightAnswers)/questions.length).toFixed(0);
	document.getElementById('answer-buttons').classList.add('no-click');
}

function setStatusClass(element, correct) {
	clearStatusClass(element);
	if(correct){
		element.classList.add('correct');
	} else {
		element.classList.add('wrong');
	}
}

function clearStatusClass(element) {
	element.classList.remove('correct');
	element.classList.remove('wrong');
}