<?php
include 'dbconnection.php';

$sql = "SELECT * FROM `questions`;";
$questionSet = array();
$i = 0;

$result = $dbConn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()){
    	$questionSet[$i]['questionText'] = $row['question_text'];
    	$questionSet[$i]['choice1'] = $row['choice_a'];
    	$questionSet[$i]['choice2'] = $row['choice_b'];
    	$questionSet[$i]['choice3'] = $row['choice_c'];
    	$questionSet[$i]['answer'] = $row['answer'];
    	$questionSet[$i]['pts'] = $row['points'];
    	$i++;
    }
}

echo json_encode($questionSet);
?>
