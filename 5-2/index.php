<!DOCTYPE html>
<!-- Display calendar of specific month. Provides 2 buttons for forwarding and backwarding month.
Default month displayed should be current month and should highlight current date. -->
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="jquery.datetimepicker.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="jquery.datetimepicker.full.js"></script>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php
		include '../5-3.php';
	?>
	<div class="container">
		<div class="controls">
			<input id="datetime" class="datetime" readonly>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#datetime").datetimepicker({
				format: 'Y/m/d',
				timepicker: false,
				closeOnDateSelect: false,
				closeOnWithoutClick: true, 
   				autoclose: false
			});
			$('#datetime').datetimepicker('show');
		});
	</script>
</body>
</html>