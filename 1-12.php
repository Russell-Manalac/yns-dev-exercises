<?php
	session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
	<?php
		include '5-3.php';
	?>
	<div class="container">
		<?php
		 	if(isset($_SESSION['username'])){	
			 	$username = $_SESSION['username'];
		 		echo '<b>user: ' . $username . '</b>&nbsp;&nbsp;&nbsp;&nbsp;'
		 		. '<a href="1-10-menu.php" class="btn btn-secondary">Main menu</a>&nbsp;'
		 		. '<a href="1-10.php" class="btn btn-success">Add data</a>&nbsp;'
		 		. '<a href="Logout.php" class="btn btn-danger">Log Out</a>&nbsp;';
		 	} else {
		 		echo "no session";
		 	}
		 	echo "<h1> View Data </h1>";
			$pageno = 1;
			$row = 1;
			$rowsPerPage = 10;	
			if (isset($_GET['pageno'])) {
				echo $_GET['pageno'] . '&nbsp;';
			    $pageno = $_GET['pageno'];
				$totalRows = count(file('personal_details.csv'));
				$startPosition = ($pageno - 1) * $rowsPerPage;
				$numberOfPages = ceil($totalRows / $rowsPerPage);
			    displayTable($pageno, $numberOfPages, $totalRows, $startPosition, $rowsPerPage);
			} else {
			    $pageno = 1;
				$totalRows = count(file('personal_details.csv'));
				$startPosition = ($pageno - 1) * $rowsPerPage;
				$numberOfPages = ceil($totalRows / $rowsPerPage);
			    displayTable($pageno, $numberOfPages, $totalRows, $startPosition, $rowsPerPage);
			}
			function displayTable($pageno, $numberOfPages, $totalRows, $startPosition, $rowsPerPage){
				$row = $startPosition;
				if($pageno > $numberOfPages) {
					$pageno = $numberOfPages;
				}
				if (($handle = fopen("personal_details.csv", "r")) !== FALSE) {
				    echo '<table border="1">';
			    	for ($i=0; $i < $row; $i++) { 
			    		if($row == 0){
			    			break;
			    		}
				    	$data = fgetcsv($handle, 1000, ",");
			    	}
				    for ($i=0; $i < $rowsPerPage; $i++) {
			   			if ($i == 0) {
				            echo '	<thead>
				            		<tr>
				            			<th>id</th>
				            			<th>full name</th>
				            			<th>email</th>
				            			<th>mobile number</th>
				            			<th>address</th>
				            			<th>gender</th>
				            			<th>image</th>
				            		</tr>
				            		</thead>
				            		<tbody>
				            		<tr>';
				        }else{
				            echo '<tr>';
				        }
				    	if(($data = fgetcsv($handle, 1000, ",")) != false) {
				   			$num = count($data);
					        for ($c=0; $c < $num; $c++) {
					            //echo $data[$c] . "<br />\n";
					            //add <img src=> at c = 6
					            if (empty($data[$c])) {
					               $value = "&nbsp;";
					            }elseif ($c == 6 && !empty($data[$c])){
					               $value = '<img src="'. $data[$c] .'" width="100" height="100">';
					            }
					            else{
					               $value = $data[$c];
					            }
				                echo '<td>'.$value.'</td>';
					        }
					        echo '</tr>';
					        $row++;
					   	}
			    	}
				    echo '</tbody></table>';
			    	fclose($handle);
				}
			}
		?>
		<ul class="pagination" style="text-align:center;">
			<li>
				<a href="?pageno=1">First</a>
			</li>
    		<li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
    			<a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
    		</li>
    		<li class="<?php if($pageno >= $numberOfPages){ echo 'disabled'; } ?>">
        		<a href="<?php if($pageno >= $numberOfPages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
    		</li>
    		<li>
    			<a href="?pageno=<?php echo $numberOfPages; ?>">Last</a>
    		</li>
		</ul>
	</div>
</body>
</html>