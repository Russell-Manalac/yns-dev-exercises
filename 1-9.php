<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
	<?php
		include '5-3.php';
	?>
	<?php
		$row = 1;
		if (($handle = fopen("personal_details.csv", "r")) !== FALSE) {
		
			echo '<table border="1">';
		
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);
				if ($row == 1) {
					echo '	<thead>
							<tr>
								<th>id</th>
								<th>full name</th>
								<th>email</th>
								<th>mobile number</th>
								<th>address</th>
								<th>gender</th>
							</tr>
							</thead>
							<tbody>
							<tr>';
				}else{
					echo '<tr>';
				}
			
				for ($c=0; $c < $num; $c++) {
					//echo $data[$c] . "<br />\n";
					if(empty($data[$c])) {
					$value = "&nbsp;";
					}else{
					$value = $data[$c];
					}
					echo '<td>'.$value.'</td>';
				}
				echo '</tr>';
				$row++;
			}
		
			echo '</tbody></table>';
			fclose($handle);
		}
	?>
</body>
</html>