<?php
	session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
	<?php
		include '5-3.php';
	?>
	<div class="container">
		<?php
			//Check Session and Display Buttons
		 	if(isset($_SESSION['username'])){	
			 	$username = $_SESSION['username'];
		 		echo '<b>user: ' . $username . '</b>&nbsp;&nbsp;&nbsp;&nbsp;'
		 		. '<a href="3-5_menu.php" class="btn btn-secondary">Main menu</a>&nbsp;'
		 		. '<a href="3-5_add.php" class="btn btn-success">Add data</a>&nbsp;'
		 		. '<a href="3-5_logout.php" class="btn btn-danger">Log Out</a>&nbsp;';
		 	} else {
		 		echo "no session";
		 	}

		 	//Declarations
			$pageno = 1;
			$row = 1;
			$rowsPerPage = 10;	

		 	//Display Table
		 	echo "<h1> View Data </h1>";
		 	//Load Table
			if (isset($_GET['pageno'])) {
				echo $_GET['pageno'] . '&nbsp;';
			    $pageno = $_GET['pageno'];
				$totalRows = getDataCount();
				$startPosition = ($pageno - 1) * $rowsPerPage;
				$numberOfPages = ceil($totalRows / $rowsPerPage);
			    displayTable($pageno, $numberOfPages, $totalRows, $startPosition, $rowsPerPage);
			} else {
			    $pageno = 1;
				$totalRows = getDataCount();
				$startPosition = ($pageno - 1) * $rowsPerPage;
				$numberOfPages = ceil($totalRows / $rowsPerPage);
			    displayTable($pageno, $numberOfPages, $totalRows, $startPosition, $rowsPerPage);
			}

			function getDataCount(){
				include "3-5_dbConnection.php";
				$sql = "SELECT COUNT(*) as 'cnt' FROM `profiles`;";

				$result = $dbConn->query($sql);
				if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();
					return $row['cnt'];
				}
			}

			//Initialize Generation of Data in the Table
			function displayTable($pageno, $numberOfPages, $totalRows, $startPosition, $rowsPerPage){
				$row = $startPosition;
				if($pageno > $numberOfPages) {
					$pageno = $numberOfPages;
				}
				include "3-5_dbConnection.php";
				$sql = "SELECT * FROM `profiles` LIMIT " . $startPosition . ", " . $rowsPerPage . ";";

				$result = $dbConn->query($sql);

				$i = 0;
				if ($result->num_rows > 0) {
				    echo '<table border="1">';
				    while($row = $result->fetch_assoc()){
			   			if ($i == 0) {
				            echo '	<thead>
				            		<tr>
				            			<th>id</th>
				            			<th>full name</th>
				            			<th>email</th>
				            			<th>mobile number</th>
				            			<th>address</th>
				            			<th>gender</th>
				            			<th>image</th>
				            		</tr>
				            		</thead>
				            		<tbody>
				            		<tr>';
				        }else{
				            echo '<tr>';
				        }
				        echo '	<td>'.$row['id'].'</td>
								<td>'.$row['full_name'].'</td>
								<td>'.$row['email'].'</td>
								<td>'.$row['mobile_number'].'</td>
								<td>'.$row['address'].'</td>
								<td>'.$row['gender'].'</td>
								<td><img src="'. $row['image_location'] .'" width="100" height="100"></td>';
				        echo '</tr>';
					   	$i++;
			    	}
				    echo '</tbody></table>';
				}
			}
		?>
		<ul class="pagination" style="text-align:center;">
			<li>
				<a href="?pageno=1">First</a>
			</li>
    		<li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
    			<a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
    		</li>
    		<li class="<?php if($pageno >= $numberOfPages){ echo 'disabled'; } ?>">
        		<a href="<?php if($pageno >= $numberOfPages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
    		</li>
    		<li>
    			<a href="?pageno=<?php echo $numberOfPages; ?>">Last</a>
    		</li>
		</ul>
	</div>
</body>
</html>