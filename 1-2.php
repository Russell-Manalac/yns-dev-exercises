<!DOCTYPE html>
<html>
<head>
   <title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
	<?php
		include '5-3.php';
	?>
   <form action="" method="get">
      <label for="value1">1st value</label>
      <input type="text" name="value1">
      <label for="value2">2nd value</label>
      <input type="text" name="value2">
      <button type="submit" name="add" value="add">add</button>
      <button type="submit" name="subtract" value="subtract">subtract</button>
      <button type="submit" name="multiply" value="multiply">multiply</button>
      <button type="submit" name="divide" value="divide">divide</button>
   </form>
   <p>answer: </p>
   <?php
   		if(isset($_GET['add'])) {
   			echo $_GET['value1'] + $_GET['value2'];
   		}else if(isset($_GET['subtract'])) {
   			echo $_GET['value1'] - $_GET['value2'];
   		}else if(isset($_GET['multiply'])) {
   			echo $_GET['value1'] * $_GET['value2'];
   		}else if(isset($_GET['divide'])) {
   			echo $_GET['value1'] / $_GET['value2'];
   		}
   ?>
</body>
</html>