<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbName = "exer3-5db";

// Create connection
$dbConn = new mysqli($servername, $username, $password, $dbName);

// Check connection
if ($dbConn->connect_error) {
  die("Connection failed: " . $dbConn->connect_error);
}
?>