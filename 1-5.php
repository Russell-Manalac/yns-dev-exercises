<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
	<?php
		include '5-3.php';
	?>
    <form action="" method="get">
       <label for="date">Date</label>
       <input type="text" name="date">
       <button type="submit" name="submit" value="submit">submit</button>
    </form>
    <?php
	    if (isset($_GET['submit'])) {
	        $date = date_create($_GET['date']);
	        date_add($date, date_interval_create_from_date_string("3 days"));
	        echo date_format($date, "Y-m-d, D");
	    }
    ?>
</body>
</html>