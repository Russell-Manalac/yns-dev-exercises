<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
	<?php
		include '5-3.php';
	?>
 	<?php
	 	if(isset($_SESSION['username'])){	
		 	$username = $_SESSION['username'];
		 	echo '<b>user: ' . $username . '</b>&nbsp;&nbsp;&nbsp;&nbsp;'
		 		. '<a href="1-10-menu.php" class="btn btn-secondary">Main menu</a>&nbsp;'
		 		. '<a href="1-12.php" class="btn btn-primary">View data</a>&nbsp;'
		 		. '<a href="Logout.php" class="btn btn-danger">Log Out</a>&nbsp;';
	 	} else {
	 		echo "no session";
	 	}
	 	$fNameError = $emailError = $numberError = $genderError = $imageError = '';
	 	$fNameToValidate = $emailToValidate = $numberToValidate = $addressToValidate = $genderToValidate = '';
	 	$rootUploads = 'uploads/';
 		if(isset($_POST['logout'])){
 			session_unset($_SESSION['username']);
 		}
 		if (isset($_POST['submit'])) {
 			//Full name Vaidation
			$target_file = $rootUploads . basename($_FILES['imageToUpload']['name']);
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			echo $target_file;
  			$identifyFile = getimagesize($_FILES["imageToUpload"]["tmp_name"]);
  			if($identifyFile !== false) {
				if($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'jpeg'
				|| $imageFileType == 'gif' ) {
					if ($_FILES['imageToUpload']['size'] < 1000000) {
						$imageError = '';
					} else {
						$imageError = 'Image too large.';
					}
				} else {
				  $imageError = 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
				}
		  	} else {
			    echo "File is not an image.";
		  	}

 			if (empty($_POST['fName'])) {
			    $fNameError = 'full name is required';
		  	} else {
			    $fNameToValidate = test_input($_POST['fName']);
			    if (!preg_match("/^[a-zA-Z-' ]*$/",$fNameToValidate)) {
			      $fNameError = 'Only letters and white space are allowed';
			    }
			}

			//Email Validation
		  	if (empty($_POST['email'])) {
		    	$emailError = 'Email is required';
		  	} else {
			    $emailToValidate = test_input($_POST['email']);

			    if (!filter_var($emailToValidate, FILTER_VALIDATE_EMAIL)) {
			   		$emailError = 'Incorrect email format';
			    }
		  	}

			//Number Validation
		  	if (empty($_POST['mNumber'])) {
		    	$numberError = 'mobile number is required';
		  	} else {
		    	$numberToValidate = test_input($_POST['mNumber']);

			    if (!is_numeric($_POST['mNumber'])) {
			   		$numberError = 'Incorrect mobile number format';
			    }
		  	}

			//Address Validation
		  	if (empty($_POST['address'])) {
		    	$address = '';
		  	} else {
		    	$addressToValidate = test_input($_POST['address']);
		  	}

			//Gender Validation
			if (empty($_POST['gender'])) {
			    $genderError = 'Gender is required';
		  	} else {
		    	$genderToValidate = test_input($_POST['gender']);
		  	}
	 		if ($fNameError == '' && $emailError == '' && $numberError == '' && $genderError == '' && $imageError == '') {
	 			auditCredentials($target_file);
	 		}
 		}

 		function auditCredentials($target_file) {
 			$file_open = fopen('personal_details.csv', 'a');
			$no_rows = count(file('personal_details.csv'));
			if ($no_rows > 1) {
		   		$no_rows = ($no_rows - 1) + 1;
		  	}
		  	move_uploaded_file($_FILES['imageToUpload']['tmp_name'], $target_file);
		  	$form_data = array(
			   'person_id'  => $no_rows,
			   'full_name'  => $_POST['fName'],
			   'email'  => $_POST['email'],
			   'mobile_number' => $_POST['mNumber'],
			   'address' => $_POST['address'],
			   'gender' => $_POST['gender'],
			   'image' => $target_file
		  	);
		  	fputcsv($file_open, $form_data);
		  	echo '<p><span style="color:green;">* data and image added successfully</span></p>';
 		}

	  	function test_input($data) {
			$data = trim($data);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			return $data;
		}
 	?>
	<div class="container">
		<h3> Add profile </h3>
		<p><span style='color:red;'>* required field</span></p>
		<form action="" method="post" enctype="multipart/form-data">
		    <label>Full Name: </label>&nbsp;
			<input type="text" name="fName">
			<span style='color:red;'>* <?php echo $fNameError;?></span>
			<br>
			<label>Email Address: </label>&nbsp;
			<input type="text" name="email">
			<span style='color:red;'>* <?php echo $emailError;?></span>
			<br>
			<label>Number: </label>&nbsp;
			<input type="text" name="mNumber">
			<span style='color:red;'>* <?php echo $numberError;?></span>
			<br>
			<label>Address: </label>&nbsp;
			<input type="text" name="address">
			<br><br>
			<input type="radio" id="male" name="gender" value="male">
			<label for="male">Male</label>
			<input type="radio" id="female" name="gender" value="female">
			<label for="female">Female</label>
			<input type="radio" id="other" name="gender" value="other">
			<label for="other">Other</label>
			<span style='color:red;'>* <?php echo $genderError;?></span>
			<br><br>
			Select image to upload:&nbsp;&nbsp;<span style='color:red;'> <?php echo $numberError;?></span>
	  		<input type="file" name="imageToUpload" id="imageToUpload">
			<br><br>
		    <button type="submit" name="submit" value="submit">submit</button>
		</form>
 	</div>
</body>
</html>