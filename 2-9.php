<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
	<?php
		include '5-3.php';
	?>

    <button type="submit" name="submit" value="submit" id="theme" onclick="renderColorTheme('red')">Red</button>
    <button type="submit" name="submit" value="submit" id="theme" onclick="renderColorTheme('blue')">Blue</button>
    <button type="submit" name="submit" value="submit" id="theme" onclick="renderColorTheme('green')">Green</button>
    <button type="submit" name="submit" value="submit" id="theme" onclick="renderColorTheme('white')">White</button>
    <br>
    <p id="txt">Me</p>
	<script type="text/javascript">
		function renderColorTheme(theme) {
			console.log(theme);
			if (theme == 'red') {
				document.getElementById("txt").style.color = 'red';
			} else if (theme == 'blue') {
				document.getElementById("txt").style.color = 'blue';
			} else if (theme == 'green') {
				document.getElementById("txt").style.color = 'green';
			} else if (theme == 'white') {
				document.getElementById("txt").style.color = 'white';
			}
		}
	</script>
</body>
</html>