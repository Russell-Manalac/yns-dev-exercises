<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<body>
</body>
	<?php
		include '5-3.php';
	?>
	<div class="container">
		<?php
			$userError = $passwordError = '';
			//Execute Login
			if (isset($_POST['login'])) {
				//Validate Credentials
	 			if (empty($_POST['user'])) {
				    $userError = 'Username is required';
			  	} 
			  	if (empty($_POST['pass'])) {
			    	$passwordError = 'Password is required';
			  	}

			  	//Proceed to MainMenu
		 		if ($userError == '' && $passwordError == '') {
					$_SESSION['username'] = $_POST['user'];
					echo $_POST['user'];
					header('Location: 3-5_menu.php');
					exit;
		 		}
			}
		?>
		<p><span style='color:black;'>Log in </span></p>
		<form method="post" enctype="multipart/form-data">
		    <label>Username: </label>&nbsp;
			<input type="text" name="user">
			<span style='color:red;'> <?php echo $userError;?></span>
			<br><br>
			<label>Password: </label>&nbsp;
			<input type="password" name="pass">
			<span style='color:red;'> <?php echo $passwordError;?></span>
			<br><br>
		    <button type="submit" name="login" value="login">login</button>
		</form>
 	</div>
</body>
</html>
