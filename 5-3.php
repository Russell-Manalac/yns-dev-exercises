<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Practice Exercise 5-3: Navigation</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="phpDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            PHP
          </a>
          <ul class="dropdown-menu" aria-labelledby="phpDropdown">
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-1.php">1-1</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-2.php">1-2</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-3.php">1-3</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-4.php">1-4</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-5.php">1-5</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-6.php">1-6</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-7.php">1-7</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-8.php">1-8</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-9.php">1-9</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-10.php">1-10</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-11.php">1-11</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-12.php">1-12</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/1-13.php">1-13</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="jsDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Javascript
          </a>
          <ul class="dropdown-menu" aria-labelledby="jsDropdown">
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-1.php">2-1</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-2.php">2-2</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-3.php">2-3</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-4.php">2-4</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-5.php">2-5</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-6.php">2-6</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-7.php">2-7</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-8.php">2-8</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-9.php">2-9</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-10.php">2-10</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-11.php">2-11</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-12.php">2-12</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-13.php">2-13</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-14.php">2-14</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/2-15.php">2-15</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="practiceDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Practice
          </a>
          <ul class="dropdown-menu" aria-labelledby="practiceDropdown">
            <li><a class="dropdown-item" href="/yns-dev-exercises/5-1/quiz.php">5-1</a></li>
            <li><a class="dropdown-item" href="/yns-dev-exercises/5-2/index.php">5-2</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<script>
    $(document).ready(function(){
      var dropdownElementList = [].slice.call(document.querySelectorAll('.dropdown-toggle'));
      var dropdownList = dropdownElementList.map(function (dropdownToggleEl) {
        return new bootstrap.Dropdown(dropdownToggleEl);
      });
    });
</script>